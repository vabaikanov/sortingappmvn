package com.task.sortingapp;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;


public class CornerTestingSorting {

    Sorting sorting = new Sorting();

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase() {
        int[] nullArray = null;
        sorting.sort(nullArray);

    }

    @Test(expected = IllegalArgumentException.class)
    public void testZeroArgsCase() {
        int[] emptyArray = {};
        sorting.sort(emptyArray);
        assertArrayEquals(new int[]{}, emptyArray);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenArgs() {
        int[] moreThantenArgsArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        Sorting.sort(moreThantenArgsArray);
        assertArrayEquals(new int[]{}, moreThantenArgsArray);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSingleElementArrayCase() {
        int[] singleElementArray = {5};
        Sorting.sort(singleElementArray);
        assertArrayEquals(new int[]{5}, singleElementArray);
    }

    @Test
    public void testSortedArraysCase() {
        int[] sortedArray = {1, 2, 3, 4, 5};
        int[] unsortedArray = {5, 4, 3, 2, 1};

        sorting.sort(sortedArray);
        assertArrayEquals(new int[]{1, 2, 3, 4, 5}, sortedArray);

        sorting.sort(unsortedArray);
        assertArrayEquals(new int[]{1, 2, 3, 4, 5}, unsortedArray);
    }

    @Test
    public void testTenArguments() {
        int[] tenArray = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        int[] expectedSortedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        sorting.sort(tenArray);
        assertArrayEquals(expectedSortedArray, tenArray);
    }
}

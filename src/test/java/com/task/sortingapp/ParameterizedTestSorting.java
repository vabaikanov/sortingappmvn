package com.task.sortingapp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;

@RunWith(Parameterized.class)
public class ParameterizedTestSorting {
    private int[] inputArray;
    private String expectedSorted;

    public ParameterizedTestSorting(int[] inputArray, String expectedSorted) {
        this.inputArray = inputArray;
        this.expectedSorted = expectedSorted;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"},
                {new int[]{100, 90, 80, 70, 60, 50, 40, 30, 20, 10}, "[10, 20, 30, 40, 50, 60, 70, 80, 90, 100]"},
                // Add more test cases as needed
        });
    }

    @Test
    public void testSorting() {
        Arrays.sort(inputArray);
        String sortedResult = Arrays.toString(inputArray);
        Assert.assertEquals(expectedSorted, sortedResult);
    }
}


package com.task.sortingapp;

import java.util.Arrays;

public class Sorting {
    public static void sort(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Array cannot be null");
        }
        if (array.length > 10) {
            throw new IllegalArgumentException("Array cannot have more than 10 characters");
        }

        if (array.length == 0) {
            throw new IllegalArgumentException("Array cannot be empty");
        }

        if (array.length == 1) {
            throw new IllegalArgumentException("Single argument array cannot be sorted");
        }


        int n = array.length;
        for (int j = 1; j < n; j++) {
            int key = array[j];
            int i = j - 1;
            while ((i > -1) && (array[i] > key)) {
                array[i + 1] = array[i];
                i--;
            }
            array[i + 1] = key;
        }
//        System.out.println(Arrays.toString(array));

    }
}




